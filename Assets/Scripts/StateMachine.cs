using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateMachine : MonoBehaviour
{
    [SerializeField] private GameObject calculator_canv;
    [SerializeField] private GameObject comparer_canv;
    private GameObject _current_canv;

    private void Start()
    {
        calculator_canv.SetActive(true);
        _current_canv = calculator_canv;
    }
    public void ChangeCanv(GameObject state)
    {
        if (_current_canv != null)
        {
            _current_canv.SetActive(false);
            state.SetActive(true);
            _current_canv=state;
        }
        
    }
}
