using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwoNumbersComparer : MonoBehaviour
{
    [SerializeField] private Text result_counter;
    [SerializeField] private InputField first_number;
    [SerializeField] private InputField second_number;

    private double _result;

    public void CompareNumbers()
    {
        if (first_number!= null && second_number != null)
        {
            result_counter.fontSize = 35;
            if (Convert.ToDouble(first_number.text)< Convert.ToDouble(second_number.text))
            {
                result_counter.text = "������";
            }
            else if (Convert.ToDouble(first_number.text) > Convert.ToDouble(second_number.text))
            {
                result_counter.text = "������";
            }
            else
            {
                result_counter.text = "�����";
            }
        }
    }
}
