using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calculator : MonoBehaviour
{
    [SerializeField] private Text result_counter;
    [SerializeField] private InputField first_Number;
    [SerializeField] private InputField second_Number;

    private double _result = 0;

    public void PlusButton()
    {
        if (first_Number != null && second_Number!=null)
        {
            _result = Convert.ToDouble(first_Number.text)+Convert.ToDouble(second_Number.text);
            Debug.Log(_result);
            result_counter.text = _result.ToString();
        }
    }

    public void MinusButton()
    {
        if (first_Number != null && second_Number != null)
        {
            _result = Convert.ToDouble(first_Number.text) - Convert.ToDouble(second_Number.text);
            Debug.Log(_result);
            result_counter.text = _result.ToString();
        }
    }

    public void MultiplyButton()
    {
        if (first_Number != null && second_Number != null)
        {
            _result = Convert.ToDouble(first_Number.text) * Convert.ToDouble(second_Number.text);
            Debug.Log(_result);
            result_counter.text = _result.ToString();
        }
    }

    public void DivideButton()
    {
        if (first_Number != null && second_Number != null)
        {
            if(Convert.ToUInt32(second_Number.text)==0)
            {
                result_counter.text = "������!";
            }
            else
            {
                _result = Convert.ToDouble(first_Number.text) / Convert.ToDouble(second_Number.text);
                Debug.Log(_result);
                result_counter.text = _result.ToString();
            }
            
        }
    }
}
